import numpy as np
import scipy.signal
from matplotlib import pyplot as plt

def zplane(p, filename, title):
    
    # plot roots in z-plane
    theta = np.linspace(0, 2*np.pi, 100)
    x1 = np.cos(theta)
    x2 = np.sin(theta)
    
    rts = np.roots(p)
    
    plt.figure(figsize=(8, 8))
    plt.scatter(rts.real, rts.imag)
    plt.plot(x1, x2)
    plt.xlim(-3, 3)
    plt.axis('equal')
    plt.ylabel('Imaginary')
    plt.xlabel('Real')
    plt.title(title, fontsize=16)
    plt.legend(['Unit Circle', 'Zeros'])
    plt.savefig(filename)

def spectral_fact(p):

    # small number for lack of precision
    sn = 0.001
    # find roots
    rts = np.roots(p)

    # find roots inside the unit circle
    r_i = rts[np.abs(rts) < 1 - sn]

    # find roots outside the unit circle
    r_o_ = rts[(((1 - sn) < np.abs(rts)) & (np.abs(rts) < 1 + sn))]
    k = np.argsort(np.angle(r_o_)).reshape((len(r_o_)/2, 2))[:, 0]
    r_o = r_o_[k]  # extract roots on the unit circle

    # concatenate all inner roots, and half of the unit length roots
    r = np.hstack((r_i, r_o))

    # construct h
    h = np.poly(r)

    # scale h
    h = h * np.sqrt(np.max(p) / np.sum(h**2))

    return h

# user input
null = input('where is the null (rad/s)?: ')
order = input('what is the order of the filter?: ')

# scalar parameter
k = 1e5

# null coefficients
null_poly = np.array([1, -2*np.cos(null), 1])

# for a classical buttersworth filter, we have M zeros at z = -1
b_z = np.poly(-1*np.ones([order-2]))

# combine classical butterworth coefficients with coefficients of desired zeros
b_z = np.polymul(b_z, null_poly)
b_inv_z = np.flipud(b_z)

# product of B(z)B(1/z)
b2_z = np.polymul(b_z, b_inv_z)

# ((1-z-1)(1-z))^m => (z^2 + 2 + 1)^m => np.poly([1, ..., 1])
g = np.poly(np.ones(b2_z.shape[0]-1))

# A(z)A(z^-1) = B(z)B(z^-1) + k(1-z^-1)(1-z)^m (eq. 10)
a2_z = np.polyadd(b2_z, k*g)

# find spectral factor of A(z)A(1/z)
a_z = spectral_fact(a2_z)

# frequency response of modified butterworth filter
H_z = scipy.signal.freqz(b_z, a_z)

# zeros of modified butterworth
zplane(b_z, 'zeros_modified_butterworth.png', 'Zeros of Modified Butterworth')

# design classical digital butterworth of the same order
b = np.poly(-1*np.ones(order))
b2 = np.convolve(b, np.flipud(b))
a2 = np.polyadd(b2, k*np.poly(np.ones(b2.shape[0]-1)))
a = spectral_fact(a2)

# frequency response of classical butterworth
H_z_classical = scipy.signal.freqz(b, a)

# plot magnitude response of modified and classical butterworth filters
plt.figure(figsize=(8, 8))
plt.plot(H_z[0], (np.abs(H_z[1])))
plt.plot(H_z_classical[0], np.abs(H_z_classical[1]))
plt.ylim(0, 1.2)
plt.xlabel('Frequency (rad/s)')
plt.ylabel('Magnitude')
plt.title('Magnitude Response of Butterworth Filters',
          fontsize=18)
plt.legend(['Modified Butterworth', 'Classical Butterworth'])
plt.savefig('frequency_response_bworth_filters.png', dpi=100)

# plot dB magnitude response of modified and classical butterworth filters
plt.figure(figsize=(8, 8))
plt.plot(H_z[0], 20*np.log((np.abs(H_z[1]))))
plt.plot(H_z_classical[0], 20*np.log(np.abs(H_z_classical[1])))
plt.ylim(-1000, 10)
plt.xlabel('Frequency (rad/s)')
plt.ylabel('Magnitude (dB)')
plt.title('Magnitude Response (dB) of Butterworth Filters', fontsize=18)
plt.legend(['Modified Butterworth', 'Classical Butterworth'])
plt.savefig('frequency_response_dB_bworth_filters.png', dpi=100)

# plot impulse response of modified butterworth filter
t, h_z = scipy.signal.dimpulse((b_z, a_z, 0.5))
plt.figure(figsize=(8, 8))
plt.stem(t, h_z[0])
plt.xlabel('Time (num samples)')
plt.ylabel('Impulse Response')
plt.title('Impulse Response of Modified Butterworth Filter', fontsize=18)
plt.savefig('impulse_response_modified_butterworth.png', dpi=100)
