import numpy as np
import scipy.signal
from matplotlib import pyplot as plt

def zplane(p, filename, title):
    
    # plot roots in z-plane
    theta = np.linspace(0, 2*np.pi, 100)
    x1 = np.cos(theta)
    x2 = np.sin(theta)
    
    rts = np.roots(p)
    
    plt.figure(figsize=(8, 8))
    plt.scatter(rts.real, rts.imag)
    plt.plot(x1, x2)
    plt.xlim(-3, 3)
    plt.axis('equal')
    plt.ylabel('Imaginary')
    plt.xlabel('Real')
    plt.title(title, fontsize=16)
    plt.legend(['Unit Circle', 'Zeros'])
    plt.savefig(filename)

def spectral_fact(p):

    # small number for lack of precision
    sn = 0.001
    # find roots
    rts = np.roots(p)

    # find roots inside the unit circle
    r_i = rts[np.abs(rts) < 1 - sn]

    # find roots outside the unit circle
    r_o_ = rts[(((1 - sn) < np.abs(rts)) & (np.abs(rts) < 1 + sn))]
    k = np.argsort(np.angle(r_o_)).reshape((len(r_o_)/2, 2))[:, 0]
    r_o = r_o_[k]  # extract roots on the unit circle

    # concatenate all inner roots, and half of the unit length roots
    r = np.hstack((r_i, r_o))

    # construct h
    h = np.poly(r)

    # scale h
    h = h * np.sqrt(np.max(p) / np.sum(h**2))

    return h


# load data
file_name = 'ecg_noisy_2.txt'
x = np.loadtxt(file_name)

'''
# user input
null = input('where is the null (rad/s)?: ')
order = input('what is the order of the filter?: ')
'''

# scalar parameter
k = 1e3

# order of the filter
order = 10
null = np.pi/4.

# null coefficients
null_poly = np.array([1, -2*np.cos(null), 1])

# right hand side of eq. (10)
b_z = scipy.signal.convolve([1, 1], [1, 1])
for i in range(order-4):
    b_z = scipy.signal.convolve(b_z, [1, 1])

# combine normal buttersworth coefficients with desired zeros
b_z = np.polymul(b_z, null_poly)
b_inv_z = np.flipud(b_z)

# product of B(z)B(1/z)
p_b = np.polymul(b_z, b_inv_z)

# right hand side of eq. (10)
g = scipy.signal.convolve([1, -1], [1, -1])
for i in range(order-2):
    g = scipy.signal.convolve(g, [1, -1])

bprod_plus_k = np.polyadd(p_b, k*g)

a_z = spectral_fact(bprod_plus_k)
       
freq_resp = scipy.signal.freqz(b_z, a_z)
 
plt.plot(freq_resp[0], (np.abs(freq_resp[1])))    

#zplane(b_z, 'zeros_modified_butterworth.png', 'Zeros of Modified Butterworth')

butter = scipy.signal.convolve([1, 1], [1, 1])
for i in range(order-2):
    butter = scipy.signal.convolve(butter, [1, 1])
freq_resp_butter = scipy.signal.freqz(butter)
plt.plot(freq_resp_butter[0], (np.abs(freq_resp_butter[1]))) 

