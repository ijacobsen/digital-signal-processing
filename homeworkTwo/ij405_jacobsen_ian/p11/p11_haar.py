import numpy as np
from matplotlib import pyplot as plt


def haar_transform(x, m):

    # define helper functions
    ave = lambda x: (1/np.sqrt(2))*(x[:, 0] + x[:, 1])
    dif = lambda x: (1/np.sqrt(2))*(x[:, 0] - x[:, 1])
    shape = lambda x: x.reshape((int(len(x)/2), 2))

    # reshape x into T/2 x 2 array
    x = shape(x)

    # peform first level of transform
    c = [ave(x)]
    d = [dif(x)]

    # complete remaining m-1 levels
    for i in range(0, m-1):
        if (len(c[i]) > 1):
            sig = shape(c[i])
            c.append(ave(sig))
            d.append(dif(sig))

    return c[-1], d


def inv_haar_transform(c, d):

    # define helper functions
    even = lambda c, d: c+d
    odd = lambda c, d: c-d
    series = lambda ev, od: np.reshape(np.vstack((ev, od)).T, (1, 2*len(ev)))[0]

    # find length of the d sequence
    m = len(d)

    # list of lists
    c = [c]

    # for each level, reconstruct the associated c
    for i in reversed(range(0, m)):
        c.insert(0, series(even(c[0], d[i]), odd(c[0], d[i])))

    # return original signal
    return c[0]


# load data
file_name = 'ecg.txt'
x = np.loadtxt(file_name)

# user decides how many levels in the transform
m = 7

# perform forward transform
c, d = haar_transform(x, m)

# calculate sum of squares of signal
ss_s = np.sum(np.power(x, 2))

# calculate sum of squares of wavelet coefficients
ss_wc = np.sum(np.power(c, 2)) + \
              np.sum([np.sum(np.power(d[i], 2)) for i in range(m)])
              
# print message
if (np.abs(ss_s - ss_wc) < 1e-4):
    print('the Parseval identity is satisfied!')
