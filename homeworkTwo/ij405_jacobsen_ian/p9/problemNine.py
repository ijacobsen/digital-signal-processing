# perform spectral factorization of a product filter which may have roots
# on the unit circle.

import numpy as np
import scipy.signal
from matplotlib import pyplot as plt

def zplane(p, filename, title):
    
    # plot roots in z-plane
    theta = np.linspace(0, 2*np.pi, 100)
    x1 = np.cos(theta)
    x2 = np.sin(theta)
    
    rts = np.roots(p)
    
    plt.figure(figsize=(8, 8))
    plt.scatter(rts.real, rts.imag)
    plt.plot(x1, x2)
    plt.xlim(-3, 3)
    plt.axis('equal')
    plt.ylabel('Imaginary')
    plt.xlabel('Real')
    plt.title(title, fontsize=16)
    plt.legend(['Unit Circle', 'Zeros'])
    plt.savefig(filename)


# define a Type-I FIR filter
p = np.array([[-3.00, 1.60, 0.17, 1.07, 3.56, 5.74, 15.36, 5.74, 3.56,
               1.07, 0.17, 1.60, -3.00]])

# small number for lack of precision
sn = 0.001

# find roots
rts = np.roots(p[0])

# find roots inside the unit circle
r_i = rts[np.abs(rts) < 1 - sn]

# find roots outside the unit circle
r_o_ = rts[(((1 - sn) < np.abs(rts)) & (np.abs(rts) < 1 + sn))]  # abs = 1
k = np.argsort(np.angle(r_o_)).reshape((len(r_o_)/2, 2))[:, 0]  # srt by angle
r_o = r_o_[k]  # extract roots on the unit circle

# concatenate all inner roots, and half of the unit length roots
r = np.hstack((r_i, r_o))

# construct h
h = np.poly(r)

# scale h
h = h * np.sqrt(np.max(p) / np.sum(h**2))

# reconstruct p via convolving h with itself flipped
p_ = np.convolve(h, np.flipud(h))

# verify that h is a spectral factor of p
error = p - p_


# plot frequency response
w, P = scipy.signal.freqz(p[0])
w = np.hstack((-np.flipud(w), w))
P = np.hstack((-np.flipud(P), P))
w_, H = scipy.signal.freqz(h)
w_ = np.hstack((-np.flipud(w_), w_))
H = np.hstack((-np.flipud(H), H))
plt.figure(figsize=(8, 8))
plt.plot(w, np.abs(P))
plt.plot(w, np.abs(H))
plt.ylabel('|A(omega)|')
plt.xlabel('omega')
plt.title('Frequency Response')
plt.legend(['|P(w)|', '|H(w)|'])
plt.savefig('p9_freq_resp.png')

# save zeros
zplane(p[0], 'p9_zeros_p.png', 'Zeros of p(n)')
zplane(h, 'p9_zeros_h.png', 'Zeros of h(n)')

# impulse response
plt.figure(figsize=(8, 8))
plt.stem(p[0])
plt.xlabel('n')
plt.ylabel('p(n)')
plt.title('Impulse Response of p(n)', fontsize=16)
plt.savefig('p9_impulse_resp_p.png')
plt.clf()
plt.figure(figsize=(8, 8))
plt.stem(h)
plt.xlabel('n')
plt.ylabel('h(n)')
plt.title('Impulse Response of h(n)', fontsize=16)
plt.savefig('p9_impulse_resp_h.png')

