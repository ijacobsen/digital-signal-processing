import numpy as np
from matplotlib import pyplot as plt
import scipy.signal

noisy = np.loadtxt('ecg_noisy.txt')
true = np.loadtxt('ecg.txt')

# sampling and cutoff frequencies
fc = 32
fs = 256

# order of filter
n = 12

# normalized cutoff frequency
Wn = fc/(fs/2.)

# numerator and denominator of transfer function
b, a = scipy.signal.butter(n, Wn)

# run filter
filt = scipy.signal.filtfilt(b, a, noisy)

# convert to seconds
seconds = np.linspace(0, len(noisy)/256, len(noisy))


# create plots
plt.figure(1)
plt.figure(figsize=(16, 16))
plt.suptitle('Cutoff Frequency = {}Hz'.format(fc), fontsize = 24)
plt.subplot(2, 1, 1)
plt.plot(seconds, noisy)
plt.plot(seconds, filt)
plt.xlabel('Time (s)')
plt.ylabel('ECG')
plt.legend(['Noisy', 'Filtered'])
plt.subplot(2, 1, 2)
plt.plot(seconds, true)
plt.plot(seconds, filt)
plt.xlabel('Time (s)')
plt.ylabel('ECG')
plt.legend(['True', 'Filtered'])
plt.savefig('p10_fc={}.png'.format(fc))
