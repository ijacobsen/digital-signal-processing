'''
implmenent STFT with 50% overlapping 
implement iSTFT
'''
import scipy
import numpy as np
from matplotlib import pyplot as plt
from scipy.io import wavfile

def stft(x, fs, R, N, w):
    
    # break up signal into blocks of size R, window and take FFT, shift
    X = scipy.array([scipy.fft(w*x[i:i+R])
                     for i in range(0, len(x) - R, N)])
    return X


def istft(X, fs, T, N, w):
    
    # how many samples we should recover
    length = T*fs
    
    # init x vector
    x = scipy.zeros(length)
    
    # get frame size
    framesamp = X.shape[1]
    
    # inverse STFT
    for n, i in enumerate(range(0, len(x) - framesamp, N)):
        x[i:i+framesamp] += scipy.real(scipy.ifft(X[n]))

    # inverse evelope to fix reconstruction error
    env = scipy.zeros(T*fs)
    for i in range(0, len(x)-framesamp, N):
        env[i:i+framesamp] += w
    env[-(length % N):] += w[-(length % N):]
    env = np.maximum(env, .01)
    return x/env

# load signal
file_name = 'noisy_speech.wav'
fs, s = wavfile.read(file_name)
x = s[:18944]

# some parameters
frame_size = 0.05
hop_size = 0.025

# block size
R = int(frame_size * fs)

# hop size
N = int(hop_size * fs)

# zero pad to remedy reconstruction errors at ends
x = np.hstack((np.zeros((R/2)), x, np.zeros((R/2))))

# how long the sample is
T = x.shape[0] * (1./fs)

# choose a window
t = np.pi/R * (np.arange(R) + 0.5)
w = scipy.hamming(R)
wind = 'Hamming'

# forward STFT
X = stft(x, fs, R, N, w)

X_ = np.copy(X)
thresh = 1000
X_[np.abs(X_) < thresh] = 0

# inverse STFT
x_ = istft(X_, fs, T, N, w)


# plot spectogram
plt.figure(figsize=(6, 6))
plt.imshow(np.abs(X_.T), aspect='auto')
plt.title('Denoised Spectogram'.format(wind))
plt.xlabel('Time')
plt.ylabel('Frequency')
plt.savefig('p3_spectogram_denoised.png')


# plot spectogram
plt.figure(figsize=(6, 6))
plt.imshow(np.abs(X.T), aspect='auto')
plt.title('Noisy Spectogram'.format(wind))
plt.xlabel('Time')
plt.ylabel('Frequency')
plt.savefig('p3_spectogram_noisy.png')

# get rid of 0 padding
x = x[hop_size*fs/2:-hop_size*fs/2]
x_ = x_[hop_size*fs/2:-hop_size*fs/2]
err = x - x_
plt.figure(figsize=(6, 6))
plt.title('Reconstruction Error with Denoised Threshold of {}'.format(thresh))
plt.plot(err)
plt.xlabel('Samples')
plt.ylabel('Error')
plt.tight_layout()
plt.savefig('p4_recon_error_c.png')


