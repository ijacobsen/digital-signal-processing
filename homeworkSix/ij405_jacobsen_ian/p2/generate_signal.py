import numpy as np
import cv2
from scipy import signal


def generate_signal(signal_type):

    # load image
    a = cv2.imread('cameraman.png', 0)

    if (signal_type == '1D'):

        # 1D signal
        x = a[200, :].reshape(a.shape[0], 1).astype(float)

        # blurring function
        M = 10
        m = np.arange(M)
        h = ((0.8)**m).reshape(M, 1)
        y = signal.convolve(x, h)

        # additive white gaussian noise
        y = y + 5*np.random.randn(y.shape[0]).reshape(y.shape[0], 1)

        return h, x, y

    if (signal_type == '2D'):

        # 2D signal
        x = a

        # blurring function
        h = np.hstack((np.ones((3, 1)), np.eye(3)))
        h = h/np.sum(h)
        y = signal.convolve2d(x, h)

        # additive white gaussian noise
        y = y + 5*np.random.randn(y.shape[0], y.shape[1])

        return h, x, y
