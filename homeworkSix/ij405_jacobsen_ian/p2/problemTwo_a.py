'''
- perform least squares deconvolution of an image that has been blurred, then
contaminated by additive white Gaussian noise.
    * deconvolution should be done via minimization of the function:
        J(x) = || h - Hx ||_2^2 + || Gx ||_2^2
    
    where G is 2D convolution by [[-1, -2, -1], [-2, 12, -2], [-1, -2, -1]]/16.
'''

import numpy as np
import generate_signal as gs
from scipy import signal
from matplotlib import pyplot as plt

def convt(h, x):
    Nh = h.shape[0]
    Nx = x.shape[0]
    f = signal.convolve(x, np.flipud(h))
    f = f[Nh-1:Nx]
    return f

H = lambda h, x: signal.convolve(x, h)
HT = lambda h, x: conv2t(h, x)
G = lambda x: signal.convolve(x, np.array([[1], [-1]]))
GT = lambda x: convt(np.array([[1], [-1]]), x)

h, x, y = gs.generate_signal('1D')
n_it = 100

# landweber iteration
x_ = np.zeros(x.shape)
cost = np.zeros((n_it, 1))
alpha = np.sum(h)**2
lambd = 1

for i in range(n_it):
    x_ = x_ + (1/alpha) * (HT(h, y) - HT(h, H(h, x_)) - lambd*GT(G(x_)))
    cost[i] = np.sum((y - H(h, x_))**2)

err = x - x_


plt.figure(figsize=(6, 10))
plt.suptitle('Deconvolution of 1D Signal with Regularization', fontsize=18)
plt.subplot(2, 1, 1)
plt.plot(x)
plt.xlabel('Time (samples)')
plt.ylabel('x(t)')
plt.title('Original Signal')
plt.subplot(2, 1, 2)
plt.plot(x_)
plt.xlabel('Time (samples)')
plt.ylabel('x_hat(t)')
plt.title('Recovered Signal')
plt.savefig('1d_deconv_regularized.png', dpi=100)

plt.figure(figsize=(5, 5))
plt.plot(err)
plt.xlabel('Time (samples)')
plt.ylabel('Error')
plt.title('Recovery Error with Regularization', fontsize=18)
plt.tight_layout()
plt.savefig('1d_deconv_error_regularized.png', dpi=100)

plt.figure(figsize=(5, 5))
plt.plot(cost)
plt.xlabel('Epochs')
plt.ylabel('Cost')
plt.title('Cost Function with Regularization', fontsize=18)
plt.tight_layout()
plt.savefig('1d_deconv_cost_regularized.png', dpi=100)

plt.figure(figsize=(5, 5))
plt.plot(np.log(x_))
plt.plot(np.log(x))
plt.plot(np.log(y))
plt.legend(['log(x_hat)', 'log(x)', 'log(y)'], loc='best')
plt.xlabel('Time (samples)')
plt.ylabel('Signal')
plt.title('x_hat, x, y with Regularization', fontsize=18)
plt.tight_layout()
plt.savefig('1d_signals_regularized.png', dpi=100)