'''
- perform least squares deconvolution of a signal that has been blurred, then
contaminated by additive white Gaussian noise.
    * deconvolution should be done via minimization of the function:
        J(x) = || h - Hx || _2^2
    * the data and the filter H are given in the zip LS deconv exercises.
'''

import numpy as np
import generate_signal as gs
from scipy import signal
from matplotlib import pyplot as plt

def convt(h, x):
    Nh = h.shape[0]
    Nx = x.shape[0]
    f = signal.convolve(x, np.flipud(h))
    f = f[Nh-1:Nx]
    return f

H = lambda h, x: signal.convolve(x, h)
HT = lambda h, x: convt(h, x)

h, x, y = gs.generate_signal('1D')
n_it = 100

# landweber iteration
x_ = np.zeros(x.shape)
cost = np.zeros((n_it, 1))
alpha = np.sum(h)**2

for i in range(n_it):
    x_ = x_ + (1/alpha) * HT(h, y - H(h, x_))
    cost[i] = np.sum((y - H(h, x_))**2)

err = x - x_

plt.figure(figsize=(6, 10))
plt.suptitle('Deconvolution of 1D Signal', fontsize=18)
plt.subplot(2, 1, 1)
plt.plot(x)
plt.xlabel('Time (samples)')
plt.ylabel('x(t)')
plt.title('Original Signal')
plt.subplot(2, 1, 2)
plt.plot(x_)
plt.xlabel('Time (samples)')
plt.ylabel('x_hat(t)')
plt.title('Recovered Signal')
plt.savefig('1d_deconv.png', dpi=100)

plt.figure(figsize=(5, 5))
plt.plot(err)
plt.xlabel('Time (samples)')
plt.ylabel('Error')
plt.title('Recovery Error', fontsize=18)
plt.tight_layout()
plt.savefig('1d_deconv_error.png', dpi=100)

plt.figure(figsize=(5, 5))
plt.plot(cost)
plt.xlabel('Epochs')
plt.ylabel('Cost')
plt.title('Cost Function', fontsize=18)
plt.tight_layout()
plt.savefig('1d_deconv_cost.png', dpi=100)

plt.figure(figsize=(5, 5))
plt.plot(np.log(x_))
plt.plot(np.log(x))
plt.plot(np.log(y))
plt.legend(['log(x_hat)', 'log(x)', 'log(y)'], loc='best')
plt.xlabel('Time (samples)')
plt.ylabel('Signal')
plt.title('x_hat, x, y', fontsize=18)
plt.tight_layout()
plt.savefig('1d_signals.png', dpi=100)
