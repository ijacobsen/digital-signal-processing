%% deconv_1D_exercise
%
%  Least squares deconvolution exercise.
%  Cameraman scan line, blurring plus noise.
%
%  Ivan Selesnick
% selesi@nyu.edu
% March 2017

%% Start

clear
% close all

%% Load data

a = imread('cameraman.png');
a = double(a);
size(a)

x = a(200, :)';

N = length(x);
n = (0:N-1)';                   % n : discrete-time index

figure(1)
clf
subplot(2, 1, 1)
imagesc(a)
title('Camerman image');
axis image
colormap('gray')

subplot(2, 1, 2)
plot(n, x)
title('Scan line of image (row 200)');
xlim([0 N])


%% Blurring function

M = 10;
m = 0:M-1;
h = (0.8).^m;
[Hf, om] = freqz(h);
dc_gain = sum(h)

figure(1)
clf
subplot(2, 1, 1)
stem(m, h)
title('Impulse response, h(n)');
xlabel('n')
xlim([-1 30])
ylim([0 1.2])

% Frequency response
subplot(2, 1, 2)
plot(om, abs(Hf))
xlabel('\omega')
title('Frequency response |H^f(\omega)|')
xlim([0 pi])

%% Blurr and noise

% Blurring
y = conv(x, h);     

% Additive white Gaussian noise (AWGN)
sigma = 10;
y = y + sigma * randn(size(y));   

figure(1)
clf
subplot(2, 1, 1)
plot(n, x)
title('True signal');
xlim([0 N])

subplot(2, 1, 2)
plot(0:N+M-2, y);
title('Blurred, noisy signal');
xlim([0 N+M])

%% Exercise
% Estimate the true signal x from the degraded signal y via least squares.


