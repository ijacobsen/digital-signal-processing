%% deconv_2D_exercise
%
%  Least squares deconvolution exercise.
%  Cameraman image, blurring plus noise.
%
%  Ivan Selesnick
% selesi@nyu.edu
% March 2017

%% Start

clear
% close all

%% Load data

x = imread('cameraman.png');
x = double(x);
size(x)

figure(1)
clf
subplot(2, 1, 1)
imagesc(x)
title('True image');
colormap('gray')
axis image
caxis([0 255])

subplot(2, 1, 2)
imagesc(x)
title('Detail')
colormap('gray')
axis image
caxis([0 255])
axis([90 160 30 90])


%% Two-dimensional impulse response 

h = [ones(3,1) eye(3)];
h = h/sum(h(:)); 

dc_gain = sum(sum(h))

h
% The impulse response is not square

%% Blurr and add noise

y = conv2(h, x);     % Two-dimensional convolution

% Additive white Gaussian noise (AWGN)
sigma = 4;
y = y + sigma * randn(size(y));   

figure(1)
clf
subplot(2, 1, 1)
imagesc(y)
title('Blurred, noisy image');
colormap('gray')
axis image
caxis([0 255])

subplot(2, 1, 2)
imagesc(y)
title('Detail')
colormap('gray')
axis image
caxis([0 255])
axis([90 160 30 90])

%% Save to file

imwrite(uint8(y), 'camerman_degraded.png')

%% Exercise
% Estimate the true image x from the degraded image y via least squares.

