%% stft_demo_2: Speech denoising using the STFT and thresholding
%
% This demo shows speech denoising via thresholding the STFT
% of the noisy speech signal.
%
%  Ivan Selesnick. NYU.

%% Noise reduction using STFT and thresholding
% We illustrate the use of the STFT to filter a noisy speech signal.
% We simulate a noisy speech signal by adding white Gaussian
% noise to a noise-free speech signal.
% The method described below is a basic form of 'Spectral Subtraction',
% a method for speech noise reduction.

[s, fs] = wavread('sp1.wav');
% [s, fs] = audioread('sp1.wav');
N = 20000;
s = s(1:N)';
T = N/fs;                          % T: duration of signal (sec)
t = (1:N)/fs;

noise = 0.01 * randn(size(s));      % Make noise
x = s + noise;                      % Add noise

plot(t, x)
title('Noisy signal')
xlabel('Time (sec.)')

soundsc(x, fs);                     % Play noisy speech signal

wavwrite(x, fs, 'noisy_speech.wav');    % Save to file
% audiowrite('noisy_speech.wav', x, fs);

%% STFT of noisy speech signal
% The STFT of the noisy speech signal is itself noisy.
% The noise is spread across time and frequency.

R = 512;
X = my_stft(x,R);

% Display STFT
imagesc([0 N/fs], [0 fs], 20*log10(abs(X)));
ylim([0 fs/2])
cmap = flipud(gray);
colormap(cmap);
caxis([-20 30])
colorbar
axis xy
xlabel('Time (sec.)')
ylabel('Frequency (Hz)')
title('Spectrogram of noisy speech signal')

%% Apply threshold in STFT domain
% Much of the noise can be eliminated by simply setting the small
% STFT coefficients to zero. We define a threshold value and set
% all values less than the threshold value to zero.
% Now the STFT is much less noisy.

Y = X;
Threshold = 0.5;
k = abs(Y) < Threshold;
Y(k) = 0;
Y(:, 1) = 0;     % remove start transients
Y(:, end) = 0;   % remove end transients

% display STFT
imagesc([0 N/fs], [0 fs], 20*log10(abs(Y)));
ylim([0 fs/2])
colormap(cmap);
caxis([-20 30])
colorbar
axis xy
xlabel('Time (sec.)')
ylabel('Frequency (Hz)')
title('Spectrogram after thresholding')

%% Denoised speech signal
% The next step is to compute the inverse STFT.  The processed speech
% is much less noisy than the noisy speech signal. However, the
% processed speech is a bit distorted. You should be able to hear
% a difference between the noise-free speech and the processed speech.
% The processed speech signal is not as crisp.

y = my_istft(Y);
y = y(1:N);
wavwrite(y, fs, 'processed_speech.wav');
% audiowrite('processed_speech.wav', y, fs);

plot(t, y)
xlabel('Time (sec.)')
title('Signal after STFT-domain denoising')

%%

soundsc(y, fs)

%% Compare noisy and processed speech fragments
% Compare a fragment of the noisy speech and
% the processed speech. The noise is visibly reduced.

subplot(2, 1, 1)
plot(t, x)
title('Noisy speech (zoom)')
xlim([0.52 0.62])

subplot(2, 1, 2)
plot(t, y)
title('Processed speech (zoom)')
xlim([0.52 0.62])


%% Lower threshold value
% To reduce distortion caused by thresholding, we may use a lower threshold value.
% This code repeats the thresholding step, but now with a smaller threshold value.
% Looking carefully at the STFT, it can be seen that more
% values in the STFT exceed the threshold and are not
% eliminated by the threshold operation.  Many of these
% values are scattered accross time and frequency; they are not part
% of the speech signal components.
% These are spurious noise spikes in the STFT.

Y = X;
Threshold = 0.3;
k = abs(Y) < Threshold;
Y(k) = 0;
Y(:,1) = 0;   % Remove start transients
Y(:,end) = 0;   % Remove end transients

% display STFT
clf
imagesc([0 N/fs], [0 fs], 20*log10(abs(Y)));
ylim([0 fs/2])
colormap(cmap);
caxis([-20 30])
colorbar
axis xy
xlabel('Time (sec.)')
ylabel('Frequency (Hz)')
title('Spectrogram after thresholding')


%% Musical noise artifact
% The spurious noise spikes in the STFT are audible as 'musical noise'
% in the processed speech signal. Listen to the signal.
%
% The choice of threshold value is therefore quite important for
% perceptual quality of this method.
% Moreover, one would like an automatic way to select a good
% threshold value.
%
% Additionally, a different threshold function can be used.
% Instead of the threshold function used here, a 'soft-threshold'
% function can be applied to the noisy STFT to reduce the musical noise.

y = my_istft(Y);
y = y(1:N);
wavwrite(y, fs, 'processed_speech_2.wav');
% audiowrite('processed_speech_2.wav', y, fs);

plot(t, y)
xlabel('Time (sec.)')
title('Processed signal')

soundsc(y, fs)

%% Noise reduction with STFT with no window
% When no window is used in the STFT (that is, the window is rectangular),
% then STFT-domain thresholding can lead to discontinuities at
% the block boundaries.
% Although it is not always very visible in the
% signal waveform, it can be audible in the reconstructed
% signal as a periodic interference.
% The audible artifact due to using no window is different than the musical noise
% artifact. The discontinuties that
% are present at the block boundaries occur at periodic
% locations (at the block boundaries) and not randomly in time-frequency
% like musical noise.

% Compute STFT
R = 512;
X = my_stft_nowin(x, R);

% Apply threshold in STFT domain
Y = X;
Threshold = 0.8;
k = abs(Y) < Threshold;
Y(k) = 0;
Y(:,1) = 0;   % remove start transients
Y(:,end) = 0;   % remove end transients

% Invert STFT
y = my_istft_nowin(Y);
y = y(1:N);
wavwrite(y, fs, 'processed_speech_nowindow.wav');
% audiowrite('processed_speech_nowindow.wav', y, fs);
soundsc(y, fs)

%%

plot(t, y)
xlabel('Time (sec.)')
title('STFT-domain denoising (no STFT window)')
xlim([0.6 0.7])
ylim([-0.3 0.3])
l1 = line(0.606 + 0.004*[0 1 1 0 0], 0.05*[-1 -1 1 1 -1], 'color', 'red');
line(0.638 + 0.004*[0 1 1 0 0], 0.05*[-1 -1 1 1 -1], 'color', 'red');
legend(l1, 'Discontinuities')

