%% deconv_demo_1
%
%  Least squares deconvolution.
%  Cameraman scanline, no noise.
%
%  Ivan Selesnick
% selesi@nyu.edu
% March 2017

%% Start

clear
% close all

%% Load data

a = imread('cameraman.png');
a = double(a);
size(a)

x = a(200, :)';

N = length(x);
n = (0:N-1)';                               % n : discrete-time index

figure(1)
clf
subplot(2, 1, 1)
imagesc(a)
title('Camerman image');
axis image
colormap('gray')

subplot(2, 1, 2)
plot(n, x)
title('Scan line of image (row 200)');
xlim([0 N])


%% Blurring function

M = 10;
m = 0:M-1;
h = (0.8).^m;

y = conv(x, h);

figure(1)
clf
subplot(2, 2, 1)
plot(n, x)
title('True signal');
xlim([0 N])

subplot(2, 2, 2)
stem(m, h)
title('Impulse response');
xlim([-1 M])
ylim([0 1.2])

subplot(2, 2, 3)
plot(0:N+M-2, y);
title('Blurred signal');

%% Frequency response
% The maximum value of the frequency response is at dc.

[Hf, om] = freqz(h);

figure(1)
clf
plot(om, abs(Hf))
xlabel('\omega')
title('Frequency resposne |H^f(\omega)|')
xlim([0 pi])

dc_gain = sum(h)

%% Convolution matrix H
% Create convolution matrix H and 
% verify that H is the same as the conv() function

H = convmtx(h', N);

% Verify that H*x is the same as y
e = conv(x, h) - H * x;   % e should be zero
max(abs(e))

%% Direct solve 

x_ds = H \ y;       % Direclty solve a system of equations 

e = x - x_ds;       % e: reconstruction error
max(abs(e))         % The true signal is perfectly recovered

%% Landweber algorithm

% Initialization
xx = zeros(size(x)); 

Nit = 200;   % Nit: Number of iterations
cost = zeros(1, Nit);

alpha = dc_gain^2   % why is this the maximum eigenvalue of H'*H ?
max(eig(H'*H))

for i = 1:Nit
    xx = xx + (1/alpha) * H' * (y - H * xx);
    cost(i) = sum((y - H*xx).^2);
end

err = x - xx;
rmse = sqrt(mean(err.^2))

% Is the RMSE different if the Landweber iteration is run for 100
% iterations?

%% Cost function

figure(2)
clf
plot(1:Nit, cost)
xlabel('Iteration')
title('Cost function value')
zoom yon
grid

%%

figure(1)
clf
subplot(2, 2, 1)
plot(n, x)
title('True signal');
xlim([0 N])

subplot(2, 2, 2)
stem(m, h)
title('Impulse response');
xlim([-1 M])
ylim([0 1.2])

subplot(2, 2, 3)
plot(0:N+M-2, y);
title('Convolved signal');

subplot(2, 2, 4)
plot(n, xx)
title('Deconvolution via Landweber iteration');
xlim([0 N])


%% Implement Landweber algorithm using operators instead of matrices

%% Define operators
% H and HT are linear operators (functions)

H = @(x) conv(h, x);
HT = @(x) convT(h, x);

% Verify that operator H is the same conv
e = conv(x, h) - H(x);   % e should be zero
err = max(abs(e))

%% Landweber Iteration

% Initialization
xx = zeros(size(x)); 
cost = zeros(1, Nit);

for i = 1:Nit
    xx = xx + (1/alpha) * HT(y - H(xx));   % H is opoerator (function), not matrix
    cost(i) = sum((y - H(xx)).^2);
end

err = x - xx;
rmse = sqrt(mean(err.^2))

