%% deconv_demo_2
%
%  Least squares deconvolution.
%  Cameraman image, no noise.
%
%  Ivan Selesnick
% selesi@nyu.edu
% March 2017

%% Start

clear
% close all

%% Load data

x = imread('cameraman.png');
x = double(x);
size(x)

% Two-dimensional impulse response 
h = [ones(3,1) eye(3)];
h = h/sum(h(:)); 
h
% The impulse response is not square

dc_gain = sum(sum(h))

%% Blurring

y = conv2(h, x);     % Two-dimensional convolution

figure(1)
clf
subplot(2, 1, 1)
imagesc(x)
title('True image');
colormap('gray')
axis image
caxis([0 255])

subplot(2, 1, 2)
imagesc(y)
title('Blurred image');
colormap('gray')
axis image
caxis([0 255])

%% Zoom in

subplot(2, 1, 1)
axis([90 160 30 90])

subplot(2, 1, 2)
axis([90 160 30 90])

%% Frequency response of 2D impulse response h
% It is informative to display the frequency response

Nf = 64;
Hf = fft2(h, Nf, Nf);
Hf = fftshift(Hf);
f = (0:Nf-1)/Nf - 0.5;

figure(1), clf
mesh(f, f, abs(Hf), 'Edgecolor', 'black')
title('Frequency response of 2D impulse response')
xlabel('f_1')
ylabel('f_2')
rotate3d on

% The maximum value is at dc.

%% Convolution operator H
% Create convolution operator H and its transpose

H = @(x) conv2(h, x);
HT = @(x) conv2T(h, x);


%% Landweber algorithm

% Initialization
xx = zeros(size(x)); 

Nit = 50;           % Nit: Number of iterations
cost = zeros(1, Nit);

alpha = dc_gain^2;  % (Why?)

for i = 1:Nit
    xx = xx + (1/alpha) * HT(y - H(xx));
    cost(i) = sum(sum((y - H(xx)).^2));
end

err = x - xx;
rmse = sqrt(mean(err(:).^2))

%% Cost function

figure(2)
clf
plot(1:Nit, cost)
xlabel('Iteration')
title('Cost function value')

%%

figure(1)
clf
subplot(2, 1, 1)
imagesc(y)
title('Blurred image');
colormap('gray')
axis image
caxis([0 255])

subplot(2, 1, 2)
imagesc(xx)
title('Deconvolution via Landweber iteration');
colormap('gray')
axis image
caxis([0 255])

%% Zoom in

subplot(2, 1, 1)
axis([90 160 30 90])

subplot(2, 1, 2)
axis([90 160 30 90])



