'''
- perform least squares deconvolution of an image that has been blurred, then
contaminated by additive white Gaussian noise.
    * deconvolution should be done via minimization of the function:
        J(x) = || h - Hx || _2^2
    * the data and the filter H are given in the zip LS deconv exercises.
'''

import numpy as np
import generate_signal as gs
from scipy import signal
from matplotlib import pyplot as plt


def conv2t(h, x):
    Nh = h.shape
    Nx = x.shape
    f = signal.convolve2d(x, np.fliplr(np.flipud(h)))
    f = f[Nh[0]-1:Nx[0], Nh[1]-1:Nx[1]]
    return f

H = lambda h, x: signal.convolve2d(x, h)
HT = lambda h, x: conv2t(h, x)

h, x, y = gs.generate_signal('2D')
n_it = 100

# landweber iteration
x_ = np.zeros(x.shape)
cost = np.zeros((n_it, 1))
alpha = np.sum(h)**2

for i in range(n_it):
    x_ = x_ + (1/alpha) * HT(h, y - H(h, x_))
    cost[i] = np.sum((y - H(h, x_))**2)

err = x - x_


plt.figure(figsize=(10, 5))
plt.suptitle('Deconvolution of 2D Signal', fontsize=18)
plt.subplot(2, 2, 1)
plt.imshow(x, cmap='gray')
plt.axis('off')
plt.title('Original Signal')
plt.subplot(2, 2, 2)
plt.imshow(x_, cmap='gray')
plt.axis('off')
plt.title('Recovered Signal')
plt.subplot(2, 2, 3)
plt.imshow(err, cmap='gray')
plt.axis('off')
plt.title('Error in Recovery')
plt.subplot(2, 2, 4)
plt.imshow(y, cmap='gray')
plt.axis('off')
plt.title('Contaminated Signal')
plt.savefig('2d_deconv.png', dpi=100)

plt.figure(figsize=(5, 5))
plt.plot(cost)
plt.xlabel('Epochs')
plt.ylabel('Cost')
plt.title('Cost of 2D Deconvolution', fontsize=18)
plt.savefig('2d_deconv_cost.png', dpi=100)
