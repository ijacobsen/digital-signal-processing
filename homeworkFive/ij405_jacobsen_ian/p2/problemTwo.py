import numpy as np
import scipy.signal
from matplotlib import pyplot as plt
import daubechies_filters as dbf

# load file
filename = 'ecg.txt'
x = np.loadtxt(filename)

# which dbt to use
k = 5

# find analysis and synthesis coefficients
[h0, h1, g0, g1] = dbf.db_filters(k)

# filter and downsample
s_0 = scipy.signal.upfirdn(h0, x, down=2)
s_1 = scipy.signal.upfirdn(h1, x, down=2)

# upsample and filter
y_0 = scipy.signal.upfirdn(g0, s_0, up=2)
y_1 = scipy.signal.upfirdn(g1, s_1, up=2)

# reconstruct signal
y = y_0 + y_1

# convolution adds length of the filter - 1 to the signal... we'll remove that
zrs = len(h0) - 1

# reconstruct original signal
x_ = y[zrs:-zrs]

# evaluate reconstruction error
plt.figure(figsize=(8,8))
plt.title('Reconstruction Error of Daubechies {} Filter'.format(k), fontsize=18)
plt.plot(x - x_)
plt.xlabel('Time (Samples)')
plt.ylabel('Reconstruction Error')
plt.savefig('reconstruction_error_db{}.png'.format(k))
