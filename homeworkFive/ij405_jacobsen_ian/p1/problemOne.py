import numpy as np
import scipy.linalg
import scipy.signal
from matplotlib import pyplot as plt


def spectral_fact(p):

    # small number for lack of precision
    sn = 0.0001
    # find roots
    rts = np.roots(p)

    # find roots inside the unit circle
    r_i = rts[np.abs(rts) < 1 - sn]

    # find roots outside the unit circle
    r_o_ = rts[(((1 - sn) < np.abs(rts)) & (np.abs(rts) < 1 + sn))]
    k = np.argsort(np.angle(r_o_)).reshape((len(r_o_)/2, 2))[:, 0]
    r_o = r_o_[k]  # extract roots on the unit circle

    # concatenate all inner roots, and half of the unit length roots
    r = np.hstack((r_i, r_o))

    # construct h
    h = np.poly(r)

    # scale h
    h = h * np.sqrt(np.max(p) / np.sum(h**2))

    return h

# 2k is the order of the polynomial
k = 5

# generate polynomial to preserve order
G = np.poly(-1*np.ones([2*k]))

# place holder for Q
Q = np.zeros([2*k - 1])

# half of convolved signal... half band... using symmetry properties
P_ = np.zeros([int(len(G) + len(Q) - 1)/2, 1])
P_[k-1, 0] = 1

# downsample rows by 2
indices = np.arange(1, 4*k-1, 2)
G_t = np.tril(scipy.linalg.toeplitz(G)[:, :2*k + 1])
G_t = np.vstack((G_t, G_t.T[1:, :]))[indices, :2*k-1]

# solve for filter coefficients
Q = np.matmul(np.linalg.inv(G_t), P_)

# final form of filter
P_z = np.convolve(G, Q.T[0])

# low-pass filter h0(n)
h_0 = spectral_fact(P_z)

# high pass filter h1(n)
h_1 = np.power(-1, np.arange(len(h_0)))*np.flipud(h_0)

# frequency response of low-pass filter
om, H_0 = scipy.signal.freqz(h_0)
om, H_1 = scipy.signal.freqz(h_1)

# frequency response of low-pass filter
om, H_0 = scipy.signal.freqz(h_0)
om, H_1 = scipy.signal.freqz(h_1)

# plot impulse response
plt.figure(figsize=(12, 24))
plt.subplot(3, 1, 1)
plt.stem(h_0)
plt.xlim([-1, len(h_0)])
plt.legend(['Low Pass Analysis Filter'])
plt.ylabel('Impulse Response')
plt.xlabel('Time (samples))')
plt.title('Impulse Response of Low Pass Analysis Filter, k={}'.format(k), fontsize=18)
plt.subplot(3, 1, 2)
plt.stem(h_1)
plt.xlim([-1, len(h_1)])
plt.legend(['High Pass Analysis Filter'])
plt.ylabel('Impulse Response')
plt.xlabel('Time (samples))')
plt.title('Impulse Response of High Pass Analysis Filter, k={}'.format(k), fontsize=18)
plt.subplot(3, 1, 3)
plt.plot(om, np.abs(H_0))
plt.plot(om, np.abs(H_1))
plt.legend(['Low Pass Analysis Filter', 'High Pass Analysis Filter'])
plt.ylabel('Magnitude Response')
plt.xlabel('Frequency (rad/s)')
plt.title('Magnitude Response of Analysis Filters, k={}'.format(k), fontsize=18)
plt.tight_layout()
plt.savefig('imp_frq_resp_k={}.png'.format(k))


# plot roots in z-plane
theta = np.linspace(0, 2*np.pi, 100)
x1 = np.cos(theta)
x2 = np.sin(theta)
rts_low = np.roots(h_0)
rts_high = np.roots(h_1)
plt.figure(figsize=(8, 8))
plt.plot(x1, x2)
plt.scatter(rts_low.real, rts_low.imag)
plt.scatter(rts_high.real, rts_high.imag)
plt.xlim(-3, 3)
plt.axis('equal')
plt.ylabel('Imaginary')
plt.xlabel('Real')
plt.title('Zeros of Filters with k={}'.format(k), fontsize=16)
plt.legend(['Unit Circle', 'Zeros of Low Pass Filter', 'Zeros of High Pass Filter'])
plt.savefig('zeros_k={}.png'.format(k))


# verify half band
plt.figure(figsize=(8, 8))
plt.stem(P_z)
plt.xlim([-1, len(P_z)])
plt.legend(['p(n)'])
plt.ylabel('Impulse Response')
plt.xlabel('Time (samples))')
plt.title('Impulse Response of Half Band Product Filter P, k={}'.format(k), fontsize=18)
plt.savefig('imp_p_resp_k={}.png'.format(k))

