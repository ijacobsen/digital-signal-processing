import numpy as np
import db_wt as dbwt
from matplotlib import pyplot as plt

# load data
filename = 'ecg.txt'
x = np.loadtxt(filename)
x = x[:1000]

# how many levels
m = 4

# which dbt to use
k = 5

# forward transform
[c, d] = dbwt.forward(x, m, k)

plt.figure(figsize=(16, 24))
plt.suptitle('Wavelet Subbands (k={})'.format(k), fontsize=18)
plt.subplot(m+1, 1, 1)
plt.stem(c, markerfmt=' ')

for i in range(1, m+1):
    plt.subplot(m+1, 1, i+1)
    plt.stem(d[m-i], markerfmt=' ', )

plt.tight_layout()
plt.savefig('wavelet_subbands_k={}.png'.format(k), dpi=100)

# reconstruction from low-pass subband
d[0] *= 0
d[1] *= 0
d[2] *= 0
d[3] *= 0

# inverse transform
x_ = dbwt.inverse(c, d, k)

plt.figure(figsize=(16, 8))
plt.subplot(2, 1, 1)
plt.plot(x)
plt.title('Signal', fontsize=18)
plt.xlabel('Time (samples)')
plt.subplot(2, 1, 2)
plt.plot(x_)
plt.title('Reconstruction from Low-Pass Subband (k={})'.format(k), fontsize=18)
plt.xlabel('Time (samples)')
plt.tight_layout()
plt.savefig('reconstruction_lowpass_k={}.png'.format(k))
