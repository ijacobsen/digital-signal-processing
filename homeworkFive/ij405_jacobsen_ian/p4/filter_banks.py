import numpy as np
import scipy.signal
from matplotlib import pyplot as plt
import daubechies_filters as dbf

# load file
filename = 'ecg.txt'
x = np.loadtxt(filename)

# which dbt to use
k = 5

# find analysis and synthesis coefficients
[h0, h1, g0, g1] = dbf.db_filters(k)

def analysis_fb(h0, h1, x):

    # low pass filter and downsample
    c = scipy.signal.upfirdn(h0, x, down=2)#[:-len(h1)/2]
    
    # high pass filter and downsample
    d = scipy.signal.upfirdn(h1, x, down=2)#[:-len(h1)/2]

    return [c, d]

def synthesis_fb(g0, g1, c, d):
    
    if ((len(d)%2 != 0) and (len(d) != len(c))):
        c = np.hstack((c, np.array([0])))
    
    # upsample and filter
    y_0 = scipy.signal.upfirdn(g0, c, up=2)#[(len(g0) - 2):]
    
    # upsample and filter 
    y_1 = scipy.signal.upfirdn(g1, d, up=2)#[(len(g0) - 2):]

    # reconstruct signal
    y = y_0 + y_1
    
    # convolution adds length of the filter - 1 to the signal... we'll remove that
    zrs = len(g0) - 1
    
    # reconstruct original signal
    x_ = y[zrs:-zrs]

    return x_
