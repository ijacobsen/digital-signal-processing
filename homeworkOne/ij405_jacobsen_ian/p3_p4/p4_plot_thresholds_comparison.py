'''
plot thresholding function
'''
import copy
import numpy as np
from matplotlib import pyplt as plt

T = 0.2
x = np.linspace(-1, 1, 1000)
x_h = copy.deepcopy(x)
x_s = copy.deepcopy(x)
x_i = copy.deepcopy(x)

x_h[np.abs(x_h) < T] = 0
      
x_s[(np.abs(x_s)) < T] = 0
x_s[(np.abs(x_s)-T)>0] += np.sign(x_s[(np.abs(x_s)-T)>0]) * (-T)

x_i[(np.abs(x_i)) < T] = np.power(14.5*T*x_i[(np.abs(x_i)) < T] , 3)
plt.plot(x, x_i)


plt.figure(2)
plt.figure(figsize=(6,8))
plt.clf()
plt.subplot(3, 1, 1)
plt.plot(x, x_h)
plt.title('Hard Thresholding')
plt.subplot(3, 1, 2)
plt.plot(x, x_s)
plt.title('Soft Thresholding')
plt.subplot(3, 1, 3)
plt.plot(x, x_i)
plt.title('Proposed Thresholding')
plt.tight_layout()
plt.savefig('Thresholding', dpi=200)
