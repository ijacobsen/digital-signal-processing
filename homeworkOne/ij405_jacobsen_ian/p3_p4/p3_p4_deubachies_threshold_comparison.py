''' 
evaluate hard and soft thresholding, using the Daubechies transform
'''
import numpy as np
from matplotlib import pyplot as plt
import copy

def db4_transform(x, m):

    def c_fcn(x):

        # pad the end of x with two 0's
        #x = np.vstack((np.zeros((2, 1)), x))
        x = np.vstack((x, np.zeros((2, 1))))

        # reshape x into T/2 x 2 array
        x = shape(x)

        # form signal matrix
        sig_mat = np.hstack((x[:-1, :], x[1:, :]))

        c = np.matmul(sig_mat, np.vstack((h0, h1, h2, h3)))

        return c

    def d_fcn(x):

        # pad the end of x with two 0's
        #x = np.vstack((np.zeros((2, 1)), x))
        x = np.vstack((x, np.zeros((2, 1))))

        # reshape x into T/2 x 2 array
        x = shape(x)

        # form signal matrix
        sig_mat = np.hstack((x[:-1, :], x[1:, :]))

        d = np.matmul(sig_mat, np.vstack((h3, -h2, h1, -h0)))

        return d

    # weights for db4
    h0, h1, h2, h3 = [(1+np.sqrt(3)), (3+np.sqrt(3)), (3-np.sqrt(3)),
                      (1-np.sqrt(3))]/(4*np.sqrt(2))

    # define helper functions
    shape = lambda x: x.reshape((len(x)/2, 2))

    # peform first level of transform
    c = [c_fcn(x)]
    d = [d_fcn(x)]

    # complete remaining m-1 levels
    for i in range(0, m-1):
        if (len(c[i]) > 1):
            sig = c[i]
            c.append(c_fcn(sig))
            d.append(d_fcn(sig))

    return c[-1], d


def db4_inv(c, d):

    def unpack(c, d):
        c = np.vstack((0, c))
        d = np.vstack((0, d))

        c_mat = np.hstack((c[1:], c[:-1]))
        d_mat = np.hstack((d[1:], d[:-1]))

        sig_mat = np.hstack((c_mat, d_mat))

        y_ev = np.matmul(sig_mat, np.vstack((h0, h2, h3, h1)))
        y_od = np.matmul(sig_mat, np.vstack((h1, h3, -h2, -h0)))

        y = np.hstack((y_ev, y_od)).reshape((y_od.shape[0]*2, 1))
        return y

    # weights for db4
    h0, h1, h2, h3 = [(1+np.sqrt(3)), (3+np.sqrt(3)), (3-np.sqrt(3)),
                      (1-np.sqrt(3))]/(4*np.sqrt(2))

    # number of levels
    m = len(d)

    # unpack
    for i in reversed(range(0, m)):
        c = unpack(c, d[i])

    return c

# load data
file_name = 'ecg_noisy.txt'
x = np.loadtxt(file_name)
x = x.reshape((len(x), 1))

# user decides how many levels in the transform
m = input('how many levels in the transform?: ')

# perform forward transform
c, d = db4_transform(x, m)

# noise level estimate
mult = 3
T1 = mult*np.std(d[-1])
T2 = mult*np.std(d[-2])

# hard thresholding
d_hard = copy.deepcopy(d)
d_hard[-1][np.abs(d_hard[-1]) < T1] = 0
d_hard[-2][np.abs(d_hard[-2]) < T2] = 0
      
# soft thresholding
d_soft = copy.deepcopy(d)
d_soft[-1][(np.abs(d[-1])) < T1] = 0
d_soft[-1][(np.abs(d[-1])-T1)>0] += np.sign(d_soft[-1][(np.abs(d[-1])-T1)>0]) * (-T1)
d_soft[-2][(np.abs(d[-2])) < T2] = 0
d_soft[-2][(np.abs(d[-2])-T2)>0] += np.sign(d_soft[-2][(np.abs(d[-2])-T2)>0]) * (-T2)


# proposed thresholding
d_ian = copy.deepcopy(d)
d_ian[-1][(np.abs(d[-1])) < T1] = np.power(10*T1*d[-1][(np.abs(d[-1])) < T1] , 3)
d_ian[-2][(np.abs(d[-2])) < T2] = np.power(10*T2*d[-2][(np.abs(d[-2])) < T2] , 3)

# perform inverse transform
y_hard = db4_inv(c, d_hard)
y_soft = db4_inv(c, d_soft)
y_ian = db4_inv(c, d_ian)

# evaluate errors
clean_data = np.loadtxt('ecg.txt')
clean_data = clean_data.reshape((len(clean_data), 1))
n = x.shape[0]
noisy_rms = np.sqrt(np.sum(np.power(clean_data-x, 2))/n)
hard_rms = np.sqrt(np.sum(np.power(clean_data-y_hard, 2))/n)
soft_rms = np.sqrt(np.sum(np.power(clean_data-y_soft, 2))/n)
ian_rms = np.sqrt(np.sum(np.power(clean_data-y_ian, 2))/n)

#########################
plt.figure(1)
plt.clf()
plt.figure(figsize=(12,8))
plt.subplot(2, 2, 1)
plt.plot(x)
plt.title('Noisy Data, RMS Error = {0:.4f}'.format(noisy_rms))
plt.xlabel('Time (n)')
plt.subplot(2, 2, 2)
plt.plot(y_ian)
plt.title('Proposed Thresh., RMS Error = {0:.4f}'.format(ian_rms))
plt.xlabel('Time (n)')
plt.subplot(2, 2, 3)
plt.plot(y_hard)
plt.title('Hard Thresh., RMS Error = {0:.4f}'.format(hard_rms))
plt.xlabel('Time (n)')
plt.subplot(2, 2, 4)
plt.plot(y_soft)
plt.title('Soft Thresh., RMS Error = {0:.4f}'.format(soft_rms))
plt.xlabel('Time (n)')
plt.tight_layout()

plt.savefig('Proposed_Denoising_Daubechies_Transform.png', dpi=100)
