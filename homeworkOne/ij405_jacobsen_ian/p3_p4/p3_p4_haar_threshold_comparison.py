''' 
evaluate hard and soft thresholding, using the Haar transform
'''
import numpy as np
from matplotlib import pyplot as plt
import copy


def haar_transform(x, m):

    # define helper functions
    ave = lambda x: 0.5*(x[:, 0] + x[:, 1])
    dif = lambda x: 0.5*(x[:, 0] - x[:, 1])
    shape = lambda x: x.reshape((len(x)/2, 2))

    # reshape x into T/2 x 2 array
    x = shape(x)

    # peform first level of transform
    c = [ave(x)]
    d = [dif(x)]

    # complete remaining m-1 levels
    for i in range(0, m-1):
        if (len(c[i]) > 1):
            sig = shape(c[i])
            c.append(ave(sig))
            d.append(dif(sig))

    return c[-1], d


def inv_haar_transform(c, d):

    # define helper functions
    even = lambda c, d: c+d
    odd = lambda c, d: c-d
    series = lambda ev, od: np.reshape(np.vstack((ev, od)).T, (1, 2*len(ev)))[0]

    # find length of the d sequence
    m = len(d)

    # list of lists
    c = [c]

    # for each level, reconstruct the associated c
    for i in reversed(range(0, m)):
        c.insert(0, series(even(c[0], d[i]), odd(c[0], d[i])))

    # return original signal
    return c[0]



# load data
file_name = 'ecg_noisy.txt'
x = np.loadtxt(file_name)

# user decides how many levels in the transform
m = input('how many levels in the transform?: ')

# perform forward transform
c, d = haar_transform(x, m)

# noise level estimate
mult = 3
T1 = mult*np.std(d[-1])
T2 = mult*np.std(d[-2])

# hard thresholding
d_hard = copy.deepcopy(d)
d_hard[-1][np.abs(d_hard[-1]) < T1] = 0
d_hard[-2][np.abs(d_hard[-2]) < T2] = 0
      
# soft thresholding
d_soft = copy.deepcopy(d)
d_soft[-1][(np.abs(d[-1])) < T1] = 0
d_soft[-1][(np.abs(d[-1])-T1)>0] += np.sign(d_soft[-1][(np.abs(d[-1])-T1)>0]) * (-T1)
d_soft[-2][(np.abs(d[-2])) < T2] = 0
d_soft[-2][(np.abs(d[-2])-T2)>0] += np.sign(d_soft[-2][(np.abs(d[-2])-T2)>0]) * (-T2)


# proposed thresholding
d_ian = copy.deepcopy(d)
d_ian[-1][(np.abs(d[-1])) < T1] = np.power(10*T1*d[-1][(np.abs(d[-1])) < T1] , 3)
d_ian[-2][(np.abs(d[-2])) < T2] = np.power(10*T2*d[-2][(np.abs(d[-2])) < T2] , 3)

# perform inverse transform
y_hard = inv_haar_transform(c, d_hard)
y_soft = inv_haar_transform(c, d_soft)
y_ian = inv_haar_transform(c, d_ian)

# evaluate errors
clean_data = np.loadtxt('ecg.txt')
n = x.shape[0]
noisy_rms = np.sqrt(np.sum(np.power(clean_data-x, 2))/n)
hard_rms = np.sqrt(np.sum(np.power(clean_data-y_hard, 2))/n)
soft_rms = np.sqrt(np.sum(np.power(clean_data-y_soft, 2))/n)
ian_rms = np.sqrt(np.sum(np.power(clean_data-y_ian, 2))/n)

#########################
plt.figure(1)
plt.clf()
plt.figure(figsize=(12,8))
plt.subplot(2, 2, 1)
plt.plot(x)
plt.title('Noisy Data, RMS Error = {0:.4f}'.format(noisy_rms))
plt.xlabel('Time (n)')
plt.subplot(2, 2, 2)
plt.plot(y_ian)
plt.title('Proposed Thresh., RMS Error = {0:.4f}'.format(ian_rms))
plt.xlabel('Time (n)')
plt.subplot(2, 2, 3)
plt.plot(y_hard)
plt.title('Hard Thresh., RMS Error = {0:.4f}'.format(hard_rms))
plt.xlabel('Time (n)')
plt.subplot(2, 2, 4)
plt.plot(y_soft)
plt.title('Soft Thresh., RMS Error = {0:.4f}'.format(soft_rms))
plt.xlabel('Time (n)')
plt.tight_layout()

plt.savefig('Denoising_Haar_Transform.png', dpi=100)
