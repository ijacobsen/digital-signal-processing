import numpy as np
from matplotlib import pyplot as plt

def db4_transform(x, m):

    def c_fcn(x):

        # pad the end of x with two 0's
        #x = np.vstack((np.zeros((2, 1)), x))
        x = np.vstack((x, np.zeros((2, 1))))

        # reshape x into T/2 x 2 array
        x = shape(x)

        # form signal matrix
        sig_mat = np.hstack((x[:-1, :], x[1:, :]))

        c = np.matmul(sig_mat, np.vstack((h0, h1, h2, h3)))

        return c

    def d_fcn(x):

        # pad the end of x with two 0's
        #x = np.vstack((np.zeros((2, 1)), x))
        x = np.vstack((x, np.zeros((2, 1))))

        # reshape x into T/2 x 2 array
        x = shape(x)

        # form signal matrix
        sig_mat = np.hstack((x[:-1, :], x[1:, :]))

        d = np.matmul(sig_mat, np.vstack((h3, -h2, h1, -h0)))

        return d

    # weights for db4
    h0, h1, h2, h3 = [(1+np.sqrt(3)), (3+np.sqrt(3)), (3-np.sqrt(3)),
                      (1-np.sqrt(3))]/(4*np.sqrt(2))

    # define helper functions
    shape = lambda x: x.reshape((len(x)/2, 2))

    # peform first level of transform
    c = [c_fcn(x)]
    d = [d_fcn(x)]

    # complete remaining m-1 levels
    for i in range(0, m-1):
        if (len(c[i]) > 1):
            sig = c[i]
            c.append(c_fcn(sig))
            d.append(d_fcn(sig))

    return c[-1], d


def db4_inv(c, d):

    def unpack(c, d):
        c = np.vstack((0, c))
        d = np.vstack((0, d))

        c_mat = np.hstack((c[1:], c[:-1]))
        d_mat = np.hstack((d[1:], d[:-1]))

        sig_mat = np.hstack((c_mat, d_mat))

        y_ev = np.matmul(sig_mat, np.vstack((h0, h2, h3, h1)))
        y_od = np.matmul(sig_mat, np.vstack((h1, h3, -h2, -h0)))

        y = np.hstack((y_ev, y_od)).reshape((y_od.shape[0]*2, 1))
        return y

    # weights for db4
    h0, h1, h2, h3 = [(1+np.sqrt(3)), (3+np.sqrt(3)), (3-np.sqrt(3)),
                      (1-np.sqrt(3))]/(4*np.sqrt(2))

    # number of levels
    m = len(d)

    # unpack
    for i in reversed(range(0, m)):
        c = unpack(c, d[i])

    return c

# load data
file_name = 'ecg_noisy.txt'
x = np.loadtxt(file_name)
x = x.reshape((len(x), 1))

# user decides how many levels in the transform
m = input('how many levels in the transform?: ')

# forward transform
c, d = db4_transform(x, m)

# inverse transform
y = db4_inv(c, d)

# evaluate the error
error = x - y
plt.figure(1)
plt.figure(figsize=(8, 8))
plt.plot(error)
plt.ylabel('Error')
plt.xlabel('Time (n)')
plt.title('Daubechies-4 Reconstruction Error')
plt.savefig('db4_reconstruction.png')
