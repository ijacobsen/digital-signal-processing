''' 
	- implement forward and inverse Haar transforms.
		- the user can specify the number of levels of the 
transform.
	- verify perfect reconstruction.
		- to do this, plot the error signal.
'''

import numpy as np
from matplotlib import pyplot as plt


def haar_transform(x, m):

    # define helper functions
    ave = lambda x: 0.5*(x[:, 0] + x[:, 1])
    dif = lambda x: 0.5*(x[:, 0] - x[:, 1])
    shape = lambda x: x.reshape((len(x)/2, 2))

    # reshape x into T/2 x 2 array
    x = shape(x)

    # peform first level of transform
    c = [ave(x)]
    d = [dif(x)]

    # complete remaining m-1 levels
    for i in range(0, m-1):
        if (len(c[i]) > 1):
            sig = shape(c[i])
            c.append(ave(sig))
            d.append(dif(sig))

    return c[-1], d


def inv_haar_transform(c, d):

    # define helper functions
    even = lambda c, d: c+d
    odd = lambda c, d: c-d
    series = lambda ev, od: np.reshape(np.vstack((ev, od)).T, (1, 2*len(ev)))[0]

    # find length of the d sequence
    m = len(d)

    # list of lists
    c = [c]

    # for each level, reconstruct the associated c
    for i in reversed(range(0, m)):
        c.insert(0, series(even(c[0], d[i]), odd(c[0], d[i])))

    # return original signal
    return c[0]


# load data
file_name = 'ecg_noisy.txt'
x = np.loadtxt(file_name)

# user decides how many levels in the transform
m = input('how many levels in the transform?: ')

# perform forward transform
c, d = haar_transform(x, m)

# perform inverse transform
y = inv_haar_transform(c, d)

# evaluate the error
error = x - y
plt.figure(1)
plt.figure(figsize=(8,8))
plt.plot(error)
plt.ylabel('Error')
plt.xlabel('Time (n)')
plt.title('Haar Reconstruction Error')
plt.savefig('haar_reconstruction.png')
