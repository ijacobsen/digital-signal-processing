import numpy as np
from matplotlib import pyplot as plt
import scipy.signal

def tvd_mm(y, lambd, Nit):
    """
    Args:
        y: column vector of the signal to be denoised
        lambd: scalar regularization term
        Nit: scalar number of iterations
    
    Returns:
        x: column vector, denoised signal
        cost: cost function history
    """

    cost = np.zeros((Nit, 1))
    N = y.shape[0]
    
    I = np.eye(N)
    D = I[1:N, :] - I[:-1]
    DDT = np.matmul(D, D.T)
    
    x = y
    Dx = np.matmul(D, x)
    Dy = np.matmul(D, y)
    
    for k in range(Nit):
        F = np.eye(N-1)
        np.fill_diagonal(F, np.abs(Dx)/lambd) 
        F += DDT
        x = y - np.matmul(D.T, np.matmul(np.linalg.inv(F), Dy))
        Dx = np.matmul(D, x)
        cost[k] = 0.5*np.sum(np.abs(x-y)**2) + lambd*np.sum(np.abs(Dx))

    return x, cost

filename_1 = 'ecg_data_noisy.txt'
filename_2 = 'blocks_noisy.txt'

data_1 = np.loadtxt(filename_1)
y_1 = data_1.reshape(len(data_1), 1)
y_1 = y_1[:256, :]
data_2 = np.loadtxt(filename_2)
y_2 = data_2.reshape(len(data_2), 1)

y_3 = y_1 + y_2

Nit = 100
lambd = 1
x_1, cost = tvd_mm(y_1, lambd, Nit)
x_2, cost = tvd_mm(y_2, lambd, Nit)
x_3, cost = tvd_mm(y_3, lambd, Nit)

x_re = x_1 + x_2

plt.figure(figsize=(8, 6))
plt.subplot(2, 1, 1)
plt.plot(x_re)
plt.ylim((-3, 9))
plt.xlabel('Time (samples)')
plt.ylabel('Signal Value')
plt.title('Added Signals After Filtering')
plt.subplot(2, 1, 2)
plt.plot(x_3)
plt.ylim((-3, 9))
plt.xlabel('Time (samples)')
plt.ylabel('Signal Value')
plt.title('Added Signals Before Filtering')
plt.tight_layout()
plt.savefig('p4_denoised.png', dpi=100)

