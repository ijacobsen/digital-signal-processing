import numpy as np
from matplotlib import pyplot as plt
import scipy.signal

def tvd_mm(y, lambd, Nit):
    """
    Args:
        y: column vector of the signal to be denoised
        lambd: scalar regularization term
        Nit: scalar number of iterations
    
    Returns:
        x: column vector, denoised signal
        cost: cost function history
    """

    cost = np.zeros((Nit, 1))
    N = y.shape[0]
    
    I = np.eye(N)
    D = I[1:N, :] - I[:-1]
    DDT = np.matmul(D, D.T)
    
    x = y
    Dx = np.matmul(D, x)
    Dy = np.matmul(D, y)
    
    for k in range(Nit):
        F = np.eye(N-1)
        np.fill_diagonal(F, np.abs(Dx)/lambd) 
        F += DDT
        x = y - np.matmul(D.T, np.matmul(np.linalg.inv(F), Dy))
        Dx = np.matmul(D, x)
        cost[k] = 0.5*np.sum(np.abs(x-y)**2) + lambd*np.sum(np.abs(Dx))

    return x, cost

filename = 'ecg_data_noisy.txt'
data = np.loadtxt(filename)
y = data.reshape(len(data), 1)

Nit = 100
lambd = 1
x, cost = tvd_mm(y, lambd, Nit)

filter_lp = scipy.signal.butter(20, 0.2)
x_ = scipy.signal.filtfilt(filter_lp[0], filter_lp[1], y.T).T

plt.figure(figsize=(8, 8))
plt.subplot(3, 1, 1)
plt.plot(y)
plt.ylim((-1, 2))
plt.xlabel('Time (samples)')
plt.ylabel('Signal Value')
plt.title('Noisy Signal')
plt.subplot(3, 1, 2)
plt.plot(x)
plt.ylim((-1, 2))
plt.xlabel('Time (samples)')
plt.ylabel('Signal Value')
plt.title('Denoised Signal with TVD')
plt.subplot(3, 1, 3)
plt.plot(x_)
plt.ylim((-1, 2))
plt.xlabel('Time (samples)')
plt.ylabel('Signal Value')
plt.title('Denoised Signal with Butterworth')
plt.tight_layout()
plt.savefig('p3_denoised.png', dpi=100)

