import numpy as np
from matplotlib import pyplot as plt
import scipy.sparse as s

def tvd_mm(y, lambd, Nit):
    """
    Args:
        y: column vector of the signal to be denoised
        lambd: scalar regularization term
        Nit: scalar number of iterations
    
    Returns:
        x: column vector, denoised signal
        cost: cost function history
    """

    cost = np.zeros((Nit, 1))
    N = y.shape[0]
    
    I = np.eye(N)
    D = I[1:N, :] - I[:-1]
    DDT = np.matmul(D, D.T)
    
    x = y
    Dx = np.matmul(D, x)
    Dy = np.matmul(D, y)
    
    for k in range(Nit):
        F = np.eye(N-1)
        np.fill_diagonal(F, np.abs(Dx)/lambd) 
        F += DDT
        x = y - np.matmul(D.T, np.matmul(np.linalg.inv(F), Dy))
        Dx = np.matmul(D, x)
        cost[k] = 0.5*np.sum(np.abs(x-y)**2) + lambd*np.sum(np.abs(Dx))

    return x, cost

filename = 'blocks_noisy.txt'
data = np.loadtxt(filename)
y = data.reshape(len(data), 1)

Nit = 100
lambd = 10
x, cost = tvd_mm(y, lambd, Nit)

plt.figure(figsize=(8, 4))
plt.subplot(2, 1, 1)
plt.plot(y)
plt.xlabel('Time (samples)')
plt.ylabel('Signal Value')
plt.title('Noisy Signal')
plt.subplot(2, 1, 2)
plt.plot(x)
plt.xlabel('Time (samples)')
plt.ylabel('Signal Value')
plt.title('Denoised Signal with Lambda = {}'.format(lambd))
plt.tight_layout()
plt.savefig('p1_lambd={}.png'.format(lambd), dpi=100)



