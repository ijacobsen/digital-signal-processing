import numpy as np
from matplotlib import pyplot as plt

lambd = 1
y = np.linspace(-3, 3, 120)
x = y
x[len(x)/2] = np.nan
phi = np.ones(len(x))
phi[len(x)/2] = 0
F = .5*(y - x)**2 + lambd*phi
name = 'minimizer_3'

plt.figure(figsize=(4, 4))
plt.title(name)
plt.plot(y, y)
plt.plot(y, x)
plt.plot(y, F)
plt.legend(['y', 'x_hat', 'F'], loc='best')
plt.savefig(name+'.png', dpi=100)
